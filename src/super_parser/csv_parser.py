import pandas as pd
from .parser import BaseParser


class CsvParser(BaseParser):
    def parse(self, path):
        return pd.read_csv(path)
