import json
from .parser import BaseParser


class JsonParser(BaseParser):
    def parse(self, path):
        return json.load(path)
