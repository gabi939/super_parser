from .json_parser import JsonParser
from .csv_parser import CsvParser


def parse(path, datatype):
    if datatype == "csv":
        return CsvParser().parse(path)
    else:
        return JsonParser().parse(path)
