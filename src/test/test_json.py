from ..super_parser import parse


def test_parse_json():
    js = "temp.json"
    dc = parse(path=js, datatype="json")
    assert isinstance(type(dc), dict)
